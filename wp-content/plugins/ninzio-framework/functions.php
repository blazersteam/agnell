<?php
/**
 * functions for ninzio framework
 *
 * @package    ninzio-framework
 * @author     Team Ninziothemes <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  2015-2016 Ninzio Framework
 */

function ninzio_framework_add_param() {
	if ( function_exists('vc_add_shortcode_param') ) {
		vc_add_shortcode_param('googlemap', 'ninzio_framework_parram_googlemap', get_template_directory_uri().'/js/admin/googlemap.js');
	}
}

function ninzio_framework_parram_googlemap($settings, $value) {
	return apply_filters( 'ninzio_framework_parram_googlemap', '
		<div id="ninzio-element-map">
			<div class="map_canvas" style="height:200px;"></div>

			<div class="vc_row-fluid googlefind">
				<input id="geocomplete" type="text" class="ninzio-location" placeholder="Type in an address" size="90" />
				<button class="button-primary find">'.esc_html__('Find', 'ninzio-framework').'</button>
			</div>

			<div class="row-fluid mapdetail">
				<div class="span6">
					<div class="wpb_element_label">'.esc_html__('Latitude', 'ninzio-framework').'</div>
					<input name="lat" class="ninzio-latgmap" type="text" value="">
				</div>

				<div class="span6">
					<div class="wpb_element_label">'.esc_html__('Longitude', 'ninzio-framework').'</div>
					<input name="lng" class="ninzio-lnggmap" type="text" value="">
				</div>
			</div>
		</div>
		');
}

function ninzio_framework_register_post_types() {
	$post_types = apply_filters( 'ninzio_framework_register_post_types', array('footer', 'brand', 'testimonial', 'megamenu') );
	if ( !empty($post_types) ) {
		foreach ($post_types as $post_type) {
			if ( file_exists( NINZIO_FRAMEWORK_DIR . 'classes/post-types/'.$post_type.'.php' ) ) {
				require NINZIO_FRAMEWORK_DIR . 'classes/post-types/'.$post_type.'.php';
			}
		}
	}
}

function ninzio_framework_widget_init() {
	$widgets = apply_filters( 'ninzio_framework_register_widgets', array() );
	if ( !empty($widgets) ) {
		foreach ($widgets as $widget) {
			if ( file_exists( NINZIO_FRAMEWORK_DIR . 'classes/widgets/'.$widget.'.php' ) ) {
				require NINZIO_FRAMEWORK_DIR . 'classes/widgets/'.$widget.'.php';
			}
		}
	}
}

function ninzio_framework_get_widget_locate( $name, $plugin_dir = NINZIO_FRAMEWORK_DIR ) {
	$template = '';
	
	// Child theme
	if ( ! $template && ! empty( $name ) && file_exists( get_stylesheet_directory() . "/widgets/{$name}" ) ) {
		$template = get_stylesheet_directory() . "/widgets/{$name}";
	}

	// Original theme
	if ( ! $template && ! empty( $name ) && file_exists( get_template_directory() . "/widgets/{$name}" ) ) {
		$template = get_template_directory() . "/widgets/{$name}";
	}

	// Plugin
	if ( ! $template && ! empty( $name ) && file_exists( $plugin_dir . "/templates/widgets/{$name}" ) ) {
		$template = $plugin_dir . "/templates/widgets/{$name}";
	}

	// Nothing found
	if ( empty( $template ) ) {
		throw new Exception( "Template /templates/widgets/{$name} in plugin dir {$plugin_dir} not found." );
	}

	return $template;
}

function ninzio_framework_get_file_contents($url, $use_include_path, $context) {
	return @file_get_contents($url, false, $context);
}

function ninzio_framework_scrape_instagram( $username ) {

    $username = strtolower( $username );
    $username = str_replace( '@', '', $username );

    if ( false === ( $instagram = get_transient( 'instagram-a5-'.sanitize_title_with_dashes( $username ) ) ) ) {

        $remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );

        if ( is_wp_error( $remote ) )
            return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'ninzio-framework' ) );

        if ( 200 != wp_remote_retrieve_response_code( $remote ) )
            return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'ninzio-framework' ) );

        $shards = explode( 'window._sharedData = ', $remote['body'] );
        $insta_json = explode( ';</script>', $shards[1] );
        $insta_array = json_decode( $insta_json[0], TRUE );

        if ( ! $insta_array )
            return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'ninzio-framework' ) );

        if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
            $images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
        } else {
            return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'ninzio-framework' ) );
        }

        if ( ! is_array( $images ) )
            return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'ninzio-framework' ) );

        $instagram = array();

        foreach ( $images as $image ) {

            $image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
            $image['display_src'] = preg_replace( '/^https?\:/i', '', $image['display_src'] );

            // handle both types of CDN url
            if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
                $image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
                $image['small'] = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
            } else {
                $urlparts = wp_parse_url( $image['thumbnail_src'] );
                $pathparts = explode( '/', $urlparts['path'] );
                array_splice( $pathparts, 3, 0, array( 's160x160' ) );
                $image['thumbnail'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
                $pathparts[3] = 's320x320';
                $image['small'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
            }

            $image['large'] = $image['thumbnail_src'];

            if ( $image['is_video'] == true ) {
                $type = 'video';
            } else {
                $type = 'image';
            }

            $caption = esc_html__( 'Instagram Image', 'ninzio-framework' );
            if ( ! empty( $image['caption'] ) ) {
                $caption = $image['caption'];
            }

            $instagram[] = array(
                'description'   => $caption,
                'link'          => trailingslashit( '//instagram.com/p/' . $image['code'] ),
                'time'          => $image['date'],
                'comments'      => $image['comments']['count'],
                'likes'         => $image['likes']['count'],
                'thumbnail'     => $image['thumbnail'],
                'small'         => $image['small'],
                'large'         => $image['large'],
                'original'      => $image['display_src'],
                'type'          => $type
            );
        }

        // do not set an empty transient - should help catch private or empty accounts
        if ( ! empty( $instagram ) ) {
            $instagram = base64_encode( serialize( $instagram ) );
            set_transient( 'instagram-a5-'.sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS*2 ) );
        }
    }

    if ( ! empty( $instagram ) ) {

        return unserialize( base64_decode( $instagram ) );

    } else {

        return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'ninzio-framework' ) );

    }
}

function ninzio_framework_images_only( $media_item ) {
    if ( $media_item['type'] == 'image' )
        return true;
    return false;
}