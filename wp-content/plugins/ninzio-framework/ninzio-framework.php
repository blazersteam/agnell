<?php
/**
 * Ninzio Framework Plugin
 *
 * A simple, truly extensible and fully responsive options framework
 * for WordPress themes and plugins. Developed with WordPress coding
 * standards and PHP best practices in mind.
 *
 * Plugin Name:     Ninzio Framework
 * Plugin URI:      http://ninziothemes.com
 * Description:     Ninzio framework for wordpress theme
 * Author:          Team NinzioTheme
 * Author URI:      http://ninziothemes.com
 * Version:         1.1
 * Text Domain:     ninzio-framework
 * License:         GPL3+
 * License URI:     http://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path:     languages
 */

define( 'NINZIO_FRAMEWORK_VERSION', '1.1');
define( 'NINZIO_FRAMEWORK_URL', plugin_dir_url( __FILE__ ) );
define( 'NINZIO_FRAMEWORK_DIR', plugin_dir_path( __FILE__ ) );

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
/**
 * Redux Framework
 *
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( NINZIO_FRAMEWORK_DIR . 'libs/redux/ReduxCore/framework.php' ) ) {
    require_once( NINZIO_FRAMEWORK_DIR . 'libs/redux/ReduxCore/framework.php' );
    require_once( NINZIO_FRAMEWORK_DIR . 'libs/loader.php' );
    define( 'NINZIO_FRAMEWORK_REDUX_ACTIVED', true );
} else {
	define( 'NINZIO_FRAMEWORK_REDUX_ACTIVED', true );
}
/**
 * Custom Post type
 *
 */
add_action( 'init', 'ninzio_framework_register_post_types', 1 );
/**
 * Import data sample
 *
 */
require NINZIO_FRAMEWORK_DIR . 'importer/import.php';
/**
 * functions
 *
 */
require NINZIO_FRAMEWORK_DIR . 'functions.php';
require NINZIO_FRAMEWORK_DIR . 'functions-preset.php';
/**
 * Widgets Core
 *
 */
require NINZIO_FRAMEWORK_DIR . 'classes/class-ninzio-widgets.php';
add_action( 'widgets_init',  'ninzio_framework_widget_init' );

require NINZIO_FRAMEWORK_DIR . 'classes/class-ninzio-megamenu.php';
require NINZIO_FRAMEWORK_DIR . 'classes/createplaceholder.php';
/**
 * Init
 *
 */
function ninzio_framework_init() {
	$demo_mode = apply_filters( 'ninzio_framework_register_demo_mode', false );
	if ( $demo_mode ) {
		ninzio_framework_init_redux();
	}
	$enable_tax_fields = apply_filters( 'ninzio_framework_enable_tax_fields', false );
	if ( $enable_tax_fields ) {
		if ( !class_exists( 'Taxonomy_MetaData_CMB2' ) ) {
			require_once NINZIO_FRAMEWORK_DIR . 'libs/cmb2/taxonomy/Taxonomy_MetaData_CMB2.php';
		}
	}
}
add_action( 'init', 'ninzio_framework_init', 100 );