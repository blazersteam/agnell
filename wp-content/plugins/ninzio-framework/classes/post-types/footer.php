<?php
/**
 * Footer manager for ninzio framework
 *
 * @package    ninzio-framework
 * @author     Team Ninziothemes <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  2015-2016 Ninzio Framework
 */
 
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

class Ninzio_PostType_Footer {

  	public static function init() {
    	add_action( 'init', array( __CLASS__, 'register_post_type' ) );
    	add_action( 'init', array( __CLASS__, 'register_footer_vc' ) );
  	}

  	public static function register_post_type() {
	    $labels = array(
			'name'                  => __( 'Ninzio Footer', 'ninzio-framework' ),
			'singular_name'         => __( 'Footer', 'ninzio-framework' ),
			'add_new'               => __( 'Add New Footer', 'ninzio-framework' ),
			'add_new_item'          => __( 'Add New Footer', 'ninzio-framework' ),
			'edit_item'             => __( 'Edit Footer', 'ninzio-framework' ),
			'new_item'              => __( 'New Footer', 'ninzio-framework' ),
			'all_items'             => __( 'All Footers', 'ninzio-framework' ),
			'view_item'             => __( 'View Footer', 'ninzio-framework' ),
			'search_items'          => __( 'Search Footer', 'ninzio-framework' ),
			'not_found'             => __( 'No Footers found', 'ninzio-framework' ),
			'not_found_in_trash'    => __( 'No Footers found in Trash', 'ninzio-framework' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Ninzio Footers', 'ninzio-framework' ),
	    );

	    register_post_type( 'ninzio_footer',
	      	array(
		        'labels'            => apply_filters( 'ninzio_postype_footer_labels' , $labels ),
		        'supports'          => array( 'title', 'editor' ),
		        'public'            => true,
		        'has_archive'       => true,
		        'menu_position'     => 51,
		        'menu_icon'         => 'dashicons-admin-home',
	      	)
	    );

  	}

  	public static function register_footer_vc() {
	    $options = get_option('wpb_js_content_types');
	    if ( is_array($options) && !in_array('ninzio_footer', $options) ) {
	      	$options[] = 'ninzio_footer';
	      	update_option( 'wpb_js_content_types', $options );
	    }
  	}
  
}

Ninzio_PostType_Footer::init();