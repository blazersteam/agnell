<?php
/**
 * mentor post type
 *
 * @package    ninzio-framework
 * @author     NinzioTheme <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  13/06/2016 NinzioTheme
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Ninzio_PostType_Team{

	/**
	 * init action and filter data to define resource post type
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_action( 'init', array( __CLASS__, 'definition_taxonomy' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
	}
	/**
	 *
	 */
	public static function definition() {
		
		$labels = array(
			'name'                  => __( 'Ninzio Teams', 'ninzio-framework' ),
			'singular_name'         => __( 'Team', 'ninzio-framework' ),
			'add_new'               => __( 'Add New Team', 'ninzio-framework' ),
			'add_new_item'          => __( 'Add New Team', 'ninzio-framework' ),
			'edit_item'             => __( 'Edit Team', 'ninzio-framework' ),
			'new_item'              => __( 'New Team', 'ninzio-framework' ),
			'all_items'             => __( 'All Teams', 'ninzio-framework' ),
			'view_item'             => __( 'View Team', 'ninzio-framework' ),
			'search_items'          => __( 'Search Team', 'ninzio-framework' ),
			'not_found'             => __( 'No Teams found', 'ninzio-framework' ),
			'not_found_in_trash'    => __( 'No Teams found in Trash', 'ninzio-framework' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Ninzio Teams', 'ninzio-framework' ),
		);

		$labels = apply_filters( 'ninzio_framework_postype_mentor_labels' , $labels );

		register_post_type( 'ninzio_team',
			array(
				'labels'            => $labels,
				'supports'          => array( 'title', 'editor', 'thumbnail' ),
				'public'            => true,
				'has_archive'       => true,
				'rewrite'           => array( 'slug' => __( 'mentor', 'ninzio-framework' ) ),
				'menu_position'     => 53,
				'categories'        => array(),
				'show_in_menu'  	=> true,
			)
		);
	}

	public static function definition_taxonomy() {
		$labels = array(
			'name'              => __( 'Team Categories', 'ninzio-framework' ),
			'singular_name'     => __( 'Team Category', 'ninzio-framework' ),
			'search_items'      => __( 'Search Team Categories', 'ninzio-framework' ),
			'all_items'         => __( 'All Team Categories', 'ninzio-framework' ),
			'parent_item'       => __( 'Parent Team Category', 'ninzio-framework' ),
			'parent_item_colon' => __( 'Parent Team Category:', 'ninzio-framework' ),
			'edit_item'         => __( 'Edit Team Category', 'ninzio-framework' ),
			'update_item'       => __( 'Update Team Category', 'ninzio-framework' ),
			'add_new_item'      => __( 'Add New Team Category', 'ninzio-framework' ),
			'new_item_name'     => __( 'New Team Category', 'ninzio-framework' ),
			'menu_name'         => __( 'Team Categories', 'ninzio-framework' ),
		);

		register_taxonomy( 'ninzio_team_category', 'ninzio_team', array(
			'labels'            => apply_filters( 'ninzio_framework_taxomony_team_category_labels', $labels ),
			'hierarchical'      => true,
			'query_var'         => 'team-category',
			'rewrite'           => array( 'slug' => __( 'team-category', 'ninzio-framework' ) ),
			'public'            => true,
			'show_ui'           => true,
		) );
	}

	/**
	 *
	 */
	public static function metaboxes( array $metaboxes ) {
		$prefix = 'ninzio_team_';
		
		$metaboxes[ $prefix . 'info' ] = array(
			'id'                        => $prefix . 'info',
			'title'                     => __( 'More Informations', 'ninzio-framework' ),
			'object_types'              => array( 'ninzio_team' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_info_fields()
		);
		
		return $metaboxes;
	}

	public static function metaboxes_info_fields() {
		$prefix = 'ninzio_team_';
		$fields = array(
			array(
				'name'              => __( 'Job', 'ninzio-framework' ),
				'id'                => $prefix . 'job',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Facebook', 'ninzio-framework' ),
				'id'                => $prefix . 'facebook',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Twitter', 'ninzio-framework' ),
				'id'                => $prefix . 'twitter',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Behance', 'ninzio-framework' ),
				'id'                => $prefix . 'behance',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Linkedin', 'ninzio-framework' ),
				'id'                => $prefix . 'linkedin',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Instagram', 'ninzio-framework' ),
				'id'                => $prefix . 'instagram',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Google Plus', 'ninzio-framework' ),
				'id'                => $prefix . 'google_plus',
				'type'              => 'text'
			),
			array(
				'name'              => __( 'Pinterest', 'ninzio-framework' ),
				'id'                => $prefix . 'pinterest',
				'type'              => 'text'
			),
		);

		return apply_filters( 'ninzio_framework_postype_ninzio_team_metaboxes_fields' , $fields );
	}

}

Ninzio_PostType_Team::init();