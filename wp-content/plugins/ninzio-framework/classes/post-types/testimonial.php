<?php
/**
 * Testimonial manager for ninzio framework
 *
 * @package    ninzio-framework
 * @author     Team Ninziothemes <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  2015-2016 Ninzio Framework
 */
 
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

class Ninzio_PostType_Testimonial {

  	public static function init() {
    	add_action( 'init', array( __CLASS__, 'register_post_type' ) );
    	add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
  	}

  	public static function register_post_type() {
	    $labels = array(
			'name'                  => __( 'Ninzio Testimonial', 'ninzio-framework' ),
			'singular_name'         => __( 'Testimonial', 'ninzio-framework' ),
			'add_new'               => __( 'Add New Testimonial', 'ninzio-framework' ),
			'add_new_item'          => __( 'Add New Testimonial', 'ninzio-framework' ),
			'edit_item'             => __( 'Edit Testimonial', 'ninzio-framework' ),
			'new_item'              => __( 'New Testimonial', 'ninzio-framework' ),
			'all_items'             => __( 'All Testimonials', 'ninzio-framework' ),
			'view_item'             => __( 'View Testimonial', 'ninzio-framework' ),
			'search_items'          => __( 'Search Testimonial', 'ninzio-framework' ),
			'not_found'             => __( 'No Testimonials found', 'ninzio-framework' ),
			'not_found_in_trash'    => __( 'No Testimonials found in Trash', 'ninzio-framework' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Ninzio Testimonials', 'ninzio-framework' ),
	    );

	    register_post_type( 'ninzio_testimonial',
	      	array(
		        'labels'            => apply_filters( 'ninzio_postype_testimonial_labels' , $labels ),
		        'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		        'public'            => true,
		        'has_archive'       => true,
		        'menu_position'     => 52
	      	)
	    );

  	}
  	
  	public static function metaboxes(array $metaboxes){
		$prefix = 'ninzio_testimonial_';
	    
	    $metaboxes[ $prefix . 'settings' ] = array(
			'id'                        => $prefix . 'settings',
			'title'                     => __( 'Testimonial Information', 'ninzio-framework' ),
			'object_types'              => array( 'ninzio_testimonial' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_fields()
		);

	    return $metaboxes;
	}

	public static function metaboxes_fields() {
		$prefix = 'ninzio_testimonial_';
	
		$fields =  array(
			array(
	            'name' => __( 'Job', 'ninzio-framework' ),
	            'id'   => "{$prefix}job",
	            'type' => 'text',
	            'description' => __('Enter Job example CEO, CTO','ninzio-framework')
          	), 
			array(
				'name' => __( 'Testimonial Link', 'ninzio-framework' ),
				'id'   => $prefix."link",
				'type' => 'text'
			)
		);  
		
		return apply_filters( 'ninzio_framework_postype_ninzio_testimonial_metaboxes_fields' , $fields );
	}
}

Ninzio_PostType_Testimonial::init();