<?php
/**
 * gallery post type
 *
 * @package    ninzio-framework
 * @author     NinzioTheme <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  13/06/2016 NinzioTheme
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Ninzio_PostType_Gallery{

	/**
	 * init action and filter data to define resource post type
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_action( 'init', array( __CLASS__, 'definition_taxonomy' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
	}
	/**
	 *
	 */
	public static function definition() {
		
		$labels = array(
			'name'                  => __( 'Ninzio Galleries', 'ninzio-framework' ),
			'singular_name'         => __( 'Gallery', 'ninzio-framework' ),
			'add_new'               => __( 'Add New Gallery', 'ninzio-framework' ),
			'add_new_item'          => __( 'Add New Gallery', 'ninzio-framework' ),
			'edit_item'             => __( 'Edit Gallery', 'ninzio-framework' ),
			'new_item'              => __( 'New Gallery', 'ninzio-framework' ),
			'all_items'             => __( 'All Galleries', 'ninzio-framework' ),
			'view_item'             => __( 'View Gallery', 'ninzio-framework' ),
			'search_items'          => __( 'Search Gallery', 'ninzio-framework' ),
			'not_found'             => __( 'No Galleries found', 'ninzio-framework' ),
			'not_found_in_trash'    => __( 'No Galleries found in Trash', 'ninzio-framework' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Ninzio Galleries', 'ninzio-framework' ),
		);

		$labels = apply_filters( 'ninzio_framework_postype_mentor_labels' , $labels );

		register_post_type( 'ninzio_gallery',
			array(
				'labels'            => $labels,
				'supports'          => array( 'title', 'thumbnail' ),
				'public'            => true,
				'has_archive'       => true,
				'rewrite'           => array( 'slug' => __( 'gallery', 'ninzio-framework' ) ),
				'menu_position'     => 53,
				'categories'        => array(),
				'show_in_menu'  	=> true,
			)
		);
	}

	public static function definition_taxonomy() {
		$labels = array(
			'name'              => __( 'Gallery Categories', 'ninzio-framework' ),
			'singular_name'     => __( 'Gallery Category', 'ninzio-framework' ),
			'search_items'      => __( 'Search Gallery Categories', 'ninzio-framework' ),
			'all_items'         => __( 'All Gallery Categories', 'ninzio-framework' ),
			'parent_item'       => __( 'Parent Gallery Category', 'ninzio-framework' ),
			'parent_item_colon' => __( 'Parent Gallery Category:', 'ninzio-framework' ),
			'edit_item'         => __( 'Edit Gallery Category', 'ninzio-framework' ),
			'update_item'       => __( 'Update Gallery Category', 'ninzio-framework' ),
			'add_new_item'      => __( 'Add New Gallery Category', 'ninzio-framework' ),
			'new_item_name'     => __( 'New Gallery Category', 'ninzio-framework' ),
			'menu_name'         => __( 'Gallery Categories', 'ninzio-framework' ),
		);

		register_taxonomy( 'ninzio_gallery_category', 'ninzio_gallery', array(
			'labels'            => apply_filters( 'ninzio_framework_taxomony_gallery_category_labels', $labels ),
			'hierarchical'      => true,
			'query_var'         => 'gallery-category',
			'rewrite'           => array( 'slug' => __( 'gallery-category', 'ninzio-framework' ) ),
			'public'            => true,
			'show_ui'           => true,
		) );
	}

	/**
	 *
	 */
	public static function metaboxes( array $metaboxes ) {
		$prefix = 'ninzio_gallery_';
		
		$metaboxes[ $prefix . 'info' ] = array(
			'id'                        => $prefix . 'info',
			'title'                     => __( 'More Informations', 'ninzio-framework' ),
			'object_types'              => array( 'ninzio_gallery' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_info_fields()
		);
		
		return $metaboxes;
	}

	public static function metaboxes_info_fields() {
		$prefix = 'ninzio_gallery_';
		$fields = array(
			array(
				'name'              => __( 'URL', 'ninzio-framework' ),
				'id'                => $prefix . 'url',
				'type'              => 'text'
			),
		);

		return apply_filters( 'ninzio_framework_postype_ninzio_gallery_metaboxes_fields' , $fields );
	}

}

Ninzio_PostType_Gallery::init();