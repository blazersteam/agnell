<?php
/**
 * Brand manager for ninzio framework
 *
 * @package    ninzio-framework
 * @author     Team Ninziothemes <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  2015-2016 Ninzio Framework
 */
 
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

class Ninzio_PostType_Brand {

  	public static function init() {
    	add_action( 'init', array( __CLASS__, 'register_post_type' ) );
    	add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
  	}

  	public static function register_post_type() {
	    $labels = array(
			'name'                  => __( 'Ninzio Brand', 'ninzio-framework' ),
			'singular_name'         => __( 'Brand', 'ninzio-framework' ),
			'add_new'               => __( 'Add New Brand', 'ninzio-framework' ),
			'add_new_item'          => __( 'Add New Brand', 'ninzio-framework' ),
			'edit_item'             => __( 'Edit Brand', 'ninzio-framework' ),
			'new_item'              => __( 'New Brand', 'ninzio-framework' ),
			'all_items'             => __( 'All Brands', 'ninzio-framework' ),
			'view_item'             => __( 'View Brand', 'ninzio-framework' ),
			'search_items'          => __( 'Search Brand', 'ninzio-framework' ),
			'not_found'             => __( 'No Brands found', 'ninzio-framework' ),
			'not_found_in_trash'    => __( 'No Brands found in Trash', 'ninzio-framework' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Ninzio Brands', 'ninzio-framework' ),
	    );

	    register_post_type( 'ninzio_brand',
	      	array(
		        'labels'            => apply_filters( 'ninzio_postype_brand_labels' , $labels ),
		        'supports'          => array( 'title', 'thumbnail' ),
		        'public'            => true,
		        'has_archive'       => true,
		        'menu_position'     => 52
	      	)
	    );

  	}
  	
  	public static function metaboxes(array $metaboxes){
		$prefix = 'ninzio_brand_';
	    
	    $metaboxes[ $prefix . 'settings' ] = array(
			'id'                        => $prefix . 'settings',
			'title'                     => __( 'Brand Information', 'ninzio-framework' ),
			'object_types'              => array( 'ninzio_brand' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_fields()
		);

	    return $metaboxes;
	}

	public static function metaboxes_fields() {
		$prefix = 'ninzio_brand_';
	
		$fields =  array(
			array(
				'name' => __( 'Brand Link', 'ninzio-framework' ),
				'id'   => $prefix."link",
				'type' => 'text'
			)
		);  
		
		return apply_filters( 'ninzio_framework_postype_ninzio_brand_metaboxes_fields' , $fields );
	}
}

Ninzio_PostType_Brand::init();