<?php
/**
 * widget base for ninzio framework
 *
 * @package    ninzio-framework
 * @author     Team Ninziothemes <ninziothemes@gmail.com >
 * @license    GNU General Public License, version 3
 * @copyright  2015-2016 Ninzio Framework
 */

abstract class Ninzio_Widget extends WP_Widget {
	
	public $template;
	abstract function getTemplate();

	public function display( $args, $instance ) {
		$this->getTemplate();
		extract($args);
		extract($instance);
		echo $before_widget;
			require ninzio_framework_get_widget_locate( $this->template );
		echo $after_widget;
	}
}